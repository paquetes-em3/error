<?php
namespace desarrollo_em3\test;
class _obj_modelo
{
    public bool $aplica_transaccion_inactivo;
    public int $registro_id;
    public string $tabla;
    public array $registro;
    public array $campos_obligatorios = array();

    public array $return_obten_data = array();

    public array $patterns = array();

    final public function obten_data(array $return= array()): array
    {
        return $this->return_obten_data;

    }

}
