<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use desarrollo_em3\liberator\liberator;
use PHPUnit\Framework\TestCase;
use stdClass;

class validaTest extends TestCase
{

    final public function test_valida_campo_obligatorio()
    {
        error::$en_error = false;
        $obj = new valida();
        $modelo = new _obj_modelo();
        $modelo->registro = array();
        $modelo->campos_obligatorios[] = 'status';
        //$modelo = new liberator($modelo);

        $result = $obj->valida_campo_obligatorio($modelo);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("Error el campo status debe existir",$result['mensaje_limpio']);
        error::$en_error = false;

        $modelo = new _obj_modelo();
        $modelo->registro = array();

        $modelo->registro['status'] = 'inactivo';
        $modelo->registro['descripcion'] = 'inactivo';
        //$modelo = new liberator($modelo);

        $result = $obj->valida_campo_obligatorio($modelo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);

        error::$en_error = false;


    }
    final public function test_valida_campo_numerico()
    {
        error::$en_error = false;
        $valida = new valida();


        $campo_numerico = '-1.1';
        $result = $valida->valida_campo_numerico($campo_numerico);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }
    final public function test_valida_campos_numericos()
    {
        error::$en_error = false;
        $valida = new valida();


        $registro = array();
        $keys = array();
        $keys[] = 'x';
        $registro['x'] = '1';
        $result = $valida->valida_campos_numericos($registro, $keys);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }
    final public function test_valida_fecha()
    {
        error::$en_error = false;
        $valida = new valida();

        $patterns = array('fecha'=>'/2020-01-01/');
        $fecha = '2020-01-01';

        $result = $valida->valida_fecha($fecha, $patterns);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

    }
    final public function test_valida_keys()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_keys($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_keys($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[a] debe existir',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_keys($keys,$data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_keys($keys,$data);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_keys($keys,$data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }
    final public function test_valida_numbers()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers($keys,$data);


        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '9.9';
        $result = $valida->valida_numbers($keys,$data);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }
    final public function test_valida_numbers_mayor_0()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-9.9';
        $result = $valida->valida_numbers_mayor_0($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0001';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0000';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo mayor a 0',$result['mensaje_limpio']);

        error::$en_error = false;


        $keys=array('a');
        $data=new stdClass();
        $data->a = '0.0001';
        $result = $valida->valida_numbers_mayor_0($keys,$data);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;

    }
    final public function test_valida_numbers_positivos()
    {
        error::$en_error = false;
        $valida = new valida();
        $keys = array();
        $data = array();
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);


        error::$en_error = false;

        $keys=array('a');
        $data=array('a');
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=array('a'=> 'zzz');
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a='xxxx';
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);
        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = new stdClass();
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error al validar $keys',$result['mensaje_limpio']);

        error::$en_error = false;

        $keys=array('a');
        $data=new stdClass();
        $data->a = '-9.9';
        $result = $valida->valida_numbers_positivos($keys,$data);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo',$result['mensaje_limpio']);
        error::$en_error = false;



        $keys=array('a');
        $data=new stdClass();
        $data->a = '-0.0001';
        $result = $valida->valida_numbers_positivos($keys,$data);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $data[$key] a Debe ser un numero positivo',$result['mensaje_limpio']);
        error::$en_error = false;

    }
    final public function test_valida_pattern()
    {
        error::$en_error = false;
        $valida = new valida();

        $key = 'a';
        $patterns = array();
        $patterns['a'] = '/x/';
        $txt = 'x';
        $result = $valida->valida_pattern($key,$patterns,$txt);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;

    }

    final public function test_valida_pattern_campo()
    {
        error::$en_error = false;
        $modelo = new _obj_modelo();
        $modelo->registro = array();
        $modelo->registro['a'] = 'x';
        $modelo->patterns['b'] = '/x/';
        $obj = new valida();

        //$modelo = new liberator($modelo);


        $key = 'a';
        $tipo_campo = 'b';

        $result = $obj->valida_pattern_campo($key,$modelo,$tipo_campo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;



    }

    final public function test_valida_pattern_modelo()
    {
        error::$en_error = false;
        $obj = new valida();
        $modelo = new _obj_modelo();
        $modelo->registro = array();
        $modelo->registro['a'] = '1';
        $modelo->patterns['id'] = "/^[1-9]+[0-9]*$/";
        $obj = new liberator($obj);


        $key = 'a';
        $tipo_campo = 'id';

        $result = $obj->valida_pattern_modelo($key,$modelo, $tipo_campo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;



    }

    final public function test_valida_transaccion_activa()
    {
        error::$en_error = false;
        $valida = new valida();

        $modelo = new _obj_modelo();
        $modelo->aplica_transaccion_inactivo = true;
        $modelo->registro_id = 1;

        $result = $valida->valida_transaccion_activa($modelo);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;

        $modelo = new _obj_modelo();
        $modelo->aplica_transaccion_inactivo = false;
        $modelo->registro_id = 1;
        $modelo->tabla = 'test';
        $modelo->return_obten_data = array('test_status'=>'activo');

        $result = $valida->valida_transaccion_activa($modelo);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);

        error::$en_error = false;

        $modelo = new _obj_modelo();
        $modelo->aplica_transaccion_inactivo = false;
        $modelo->registro_id = 1;
        $modelo->tabla = 'test';
        $modelo->return_obten_data = array('test_status'=>'inactivo');

        $result = $valida->valida_transaccion_activa($modelo);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        error::$en_error = false;

    }
    final public function test_valida_values_del_array()
    {
        error::$en_error = false;
        $valida = new valida();


        $arreglo = array();
        $arreglo_keys = array();
        $arreglo_values = array();

        $arreglo_keys[] = 'a';
        $arreglo_values[] = 'val_a';
        $arreglo['a'] = 'val_a';

        $result = $valida->valida_values_del_array($arreglo,$arreglo_keys,$arreglo_values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);


        error::$en_error = false;

    }

}
