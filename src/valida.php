<?php
namespace desarrollo_em3\error;
class valida{

    private error $error;

    public function __construct()
    {
        $this->error = new error();

    }

    /**
     * EM3
     * Valida que los campos obligatorios estén presentes y no vacíos en un modelo.
     *
     * Este método recorre los campos obligatorios definidos en `$modelo->campos_obligatorios` y verifica que:
     * 1. Cada campo obligatorio exista en `$modelo->registro`.
     * 2. El valor del campo no sea una cadena vacía.
     *
     * Si algún campo no cumple con estas condiciones, genera un error detallado.
     *
     * ### Propósito
     * Garantizar la integridad de los datos en un registro, asegurando que todos los campos obligatorios estén definidos
     * y contengan valores válidos.
     *
     * @param object $modelo Objeto que contiene:
     *                       - `campos_obligatorios` (array): Lista de campos obligatorios a validar.
     *                       - `registro` (array): Datos del registro que se están validando.
     *
     * @return array Devuelve un array con los campos obligatorios si todos son válidos.
     *               En caso de error, retorna un array con los detalles del error.
     *
     * ### Ejemplo de uso
     *
     * #### Caso exitoso: Todos los campos presentes y válidos
     * ```php
     * $modelo = (object)[
     *     'campos_obligatorios' => ['nombre', 'edad', 'correo'],
     *     'registro' => [
     *         'nombre' => 'Juan Pérez',
     *         'edad' => 30,
     *         'correo' => 'juan.perez@example.com'
     *     ]
     * ];
     *
     * $validador = new ClaseValidadora();
     * $resultado = $validador->valida_campo_obligatorio($modelo);
     *
     * // Resultado esperado:
     * // ['nombre', 'edad', 'correo']
     * ```
     *
     * #### Caso: Falta un campo obligatorio
     * ```php
     * $modelo = (object)[
     *     'campos_obligatorios' => ['nombre', 'edad', 'correo'],
     *     'registro' => [
     *         'nombre' => 'Juan Pérez',
     *         'edad' => 30
     *     ]
     * ];
     *
     * $validador = new ClaseValidadora();
     * $resultado = $validador->valida_campo_obligatorio($modelo);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error el campo correo debe existir',
     * //     'data' => [
     * //         'registro' => ['nombre' => 'Juan Pérez', 'edad' => 30],
     * //         'campos_obligatorios' => ['nombre', 'edad', 'correo']
     * //     ]
     * // ]
     * ```
     *
     * #### Caso: Campo presente pero vacío
     * ```php
     * $modelo = (object)[
     *     'campos_obligatorios' => ['nombre', 'edad', 'correo'],
     *     'registro' => [
     *         'nombre' => 'Juan Pérez',
     *         'edad' => 30,
     *         'correo' => ''
     *     ]
     * ];
     *
     * $validador = new ClaseValidadora();
     * $resultado = $validador->valida_campo_obligatorio($modelo);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error el campo correo no puede venir vacio',
     * //     'data' => [
     * //         'registro' => ['nombre' => 'Juan Pérez', 'edad' => 30, 'correo' => ''],
     * //         'campos_obligatorios' => ['nombre', 'edad', 'correo']
     * //     ]
     * // ]
     * ```
     *
     * @note Este método asume que `$modelo` contiene las propiedades `campos_obligatorios` y `registro`.
     */
    final public function valida_campo_obligatorio(object $modelo): array
    {

        foreach($modelo->campos_obligatorios as $campo_obligatorio){

            $campo_obligatorio = trim($campo_obligatorio);

            if(!key_exists($campo_obligatorio,$modelo->registro)){

                return (new error())->error(
                    'Error el campo '.$campo_obligatorio.' debe existir',
                    [$modelo->registro,$modelo->campos_obligatorios]);

            }
            if((string)$modelo->registro[$campo_obligatorio] === ''){
                return (new error())->error(
                    'Error el campo '.$campo_obligatorio.' no puede venir vacio',
                    [$modelo->registro,$modelo->campos_obligatorios]);
            }
        }

        return $modelo->campos_obligatorios;

    }

    /**
     * EM3
     * Valida que un campo sea numérico y no esté vacío.
     *
     * Esta función verifica si un campo proporcionado es numérico y no está vacío.
     * Si no cumple estas condiciones, devuelve un array con detalles del error.
     *
     * @param mixed $campo_numerico El valor del campo a validar.
     *
     * @return bool|array Devuelve `true` si el campo es numérico y no está vacío.
     *                    En caso contrario, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validar un campo numérico válido
     * $resultado = $this->valida_campo_numerico(12345);
     * // Resultado: true
     *
     * // Ejemplo 2: Validar un campo vacío
     * $resultado = $this->valida_campo_numerico('');
     * // Resultado:
     * // ['error' => 'Error campo_numerico esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Validar un campo no numérico
     * $resultado = $this->valida_campo_numerico('abc123');
     * // Resultado:
     * // ['error' => 'Error abc123 debe ser numerico', 'data' => 'abc123']
     * ```
     */
    final public function valida_campo_numerico($campo_numerico)
    {
        // Eliminar espacios alrededor del valor
        $campo_numerico = trim($campo_numerico);

        // Validar que el campo no esté vacío
        if ($campo_numerico === '') {
            return $this->error->error('Error campo_numerico esta vacio', $campo_numerico);
        }

        // Validar que el campo sea numérico
        if (!is_numeric($campo_numerico)) {
            return $this->error->error('Error ' . $campo_numerico . ' debe ser numerico', $campo_numerico);
        }

        // Si pasa todas las validaciones, retornar true
        return true;
    }


    /**
     * EM3
     * Valida que ciertos campos de un registro sean numéricos y no estén vacíos.
     *
     * Esta función verifica si un conjunto de claves específicas en `$registro` existen,
     * y luego valida que sus valores sean numéricos y no estén vacíos.
     *
     * @param array $registro El array de datos donde se validarán las claves.
     * @param array $keys Un array con las claves a validar en `$registro`.
     *
     * @return bool|array Devuelve `true` si todos los campos son numéricos y válidos. En caso de error,
     *                    devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validar campos numéricos en un registro válido
     * $registro = ['campo1' => 100, 'campo2' => 200, 'campo3' => 300];
     * $keys = ['campo1', 'campo2', 'campo3'];
     * $resultado = $this->valida_campos_numericos($registro, $keys);
     * // Resultado: true
     *
     * // Ejemplo 2: Registro con una clave faltante
     * $registro = ['campo1' => 100, 'campo2' => 200];
     * $keys = ['campo1', 'campo2', 'campo3'];
     * $resultado = $this->valida_campos_numericos($registro, $keys);
     * // Resultado:
     * // ['error' => 'Error campo invalido', 'data' => [...]]
     *
     * // Ejemplo 3: Campo no numérico
     * $registro = ['campo1' => 100, 'campo2' => 'texto', 'campo3' => 300];
     * $keys = ['campo1', 'campo2', 'campo3'];
     * $resultado = $this->valida_campos_numericos($registro, $keys);
     * // Resultado:
     * // ['error' => 'Error al validar campo numerico.', 'data' => [...]]
     * ```
     */
    final public function valida_campos_numericos(array $registro, array $keys)
    {
        // Validar que las claves existan en el registro
        $valida_existencias = $this->valida_keys($keys, $registro);
        if (error::$en_error) {
            return $this->error->error('Error campo invalido', $valida_existencias);
        }

        // Validar que los valores de las claves sean numéricos
        foreach ($keys as $key) {
            $valida_numerico = $this->valida_campo_numerico($registro[$key]);
            if (error::$en_error) {
                $mensaje = 'Error al validar campo numerico.';
                return $this->error->error($mensaje, $valida_numerico);
            }
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


    /**
     * TRASLADADO
     * Valida una fecha contra un conjunto de patrones especificados.
     *
     * Esta función verifica si una fecha dada cumple con uno de los patrones especificados.
     * Si la fecha es válida, retorna un mensaje indicando que la fecha es válida. Si no,
     * retorna un error indicando que la fecha es inválida.
     *
     * @param string $fecha La fecha a validar en formato de cadena.
     * @param array $patterns Un arreglo de patrones de validación.
     * @return array Un arreglo que contiene un mensaje de éxito si la fecha es válida, o un error en caso contrario.
     */
    final public function valida_fecha(string $fecha, array $patterns): array
    {
        $valida  = $this->valida_pattern('fecha',$patterns,$fecha);
        if(error::$en_error){
            return $this->error->error('Error fecha invalida, valor correcto [YYYY-MM-DD]', $valida);
        }
        if(!$valida){
            return $this->error->error('Error fecha invalida, valor correcto [YYYY-MM-DD]', $fecha);
        }
        return array('mensaje'=>'fecha valida');
    }

    /**
     * EM3
     * Valida que un conjunto de claves exista en un array o un objeto y, opcionalmente, que sus valores no estén vacíos.
     *
     * Esta función verifica que todas las claves especificadas en `$keys` existan en `$data`, y que, si `$valida_vacios`
     * está activado, sus valores no sean cadenas vacías.
     *
     * @param array $keys Un array de claves que se espera validar en `$data`.
     * @param array|object $data Los datos donde se validarán las claves. Puede ser un array o un objeto.
     * @param bool $valida_vacios (Opcional) Si es `true`, también valida que los valores no estén vacíos. Por defecto es `true`.
     *
     * @return bool|array Devuelve `true` si todas las claves existen (y opcionalmente no están vacías). Si alguna clave no existe
     *                    o está vacía (según la configuración), devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validar claves en un array
     * $keys = ['nombre', 'edad', 'email'];
     * $data = ['nombre' => 'Juan', 'edad' => 30, 'email' => 'juan@example.com'];
     * $resultado = $this->valida_keys($keys, $data);
     * // Resultado: true
     *
     * // Ejemplo 2: Validar claves con valores vacíos
     * $keys = ['nombre', 'edad', 'email'];
     * $data = ['nombre' => '', 'edad' => 30, 'email' => ''];
     * $resultado = $this->valida_keys($keys, $data);
     * // Resultado:
     * // ['error' => 'Error $data[nombre] no puede venir vacio', 'data' => [...]]
     *
     * // Ejemplo 3: Validar claves en un objeto
     * $keys = ['nombre', 'edad'];
     * $data = (object)['nombre' => 'Ana', 'edad' => 25];
     * $resultado = $this->valida_keys($keys, $data);
     * // Resultado: true
     *
     * // Ejemplo 4: Clave faltante
     * $keys = ['nombre', 'edad'];
     * $data = ['nombre' => 'Carlos'];
     * $resultado = $this->valida_keys($keys, $data);
     * // Resultado:
     * // ['error' => 'Error $data[edad] debe existir', 'data' => [...]]
     * ```
     */
    final public function valida_keys(array $keys, $data, bool $valida_vacios = true)
    {
        // Verificar que $keys no esté vacío
        if (count($keys) === 0) {
            return $this->error->error('Error $keys debe tener datos', $keys);
        }

        // Convertir $data a array si es un objeto
        if (is_object($data)) {
            $data = (array) $data;
        }

        // Verificar que todas las claves existan en $data
        foreach ($keys as $key) {
            if (!isset($data[$key])) {
                return $this->error->error('Error $data[' . $key . '] debe existir', $data);
            }
        }

        // Validar que las claves no estén vacías (si aplica)
        if ($valida_vacios) {
            foreach ($keys as $key) {
                if (!is_string($data[$key])) {
                    continue;
                }
                if ($data[$key] === '') {
                    return $this->error->error('Error $data[' . $key . '] no puede venir vacio', $data);
                }
            }
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


    /**
     * TRASLADADO
     * Valida que los valores de las claves especificadas en un array sean numéricos.
     *
     * Esta función toma un array de claves y un conjunto de datos. Valida que las claves
     * especificadas existan en los datos y que sus valores sean numéricos. Si ocurre un error
     * durante la validación de las claves o si algún valor no es numérico, se devuelve un array
     * con el mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir y ser numéricas en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen y sus valores son numéricos.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }

        if(is_object($data)){
            $data = (array) $data;
        }

        foreach($keys as $key){
            if (!is_numeric($data[$key])) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero', $data);
            }
        }
        return true;

    }

    /**
     * TRASLADADO
     * Valida que los valores de las claves especificadas en un array sean números mayores a 0.
     *
     * Esta función toma un array de claves y un conjunto de datos. Primero valida que las claves
     * especificadas existan en los datos, que sus valores sean numéricos y positivos. Luego, verifica
     * que los valores sean mayores a 0. Si ocurre un error durante alguna de las validaciones, se
     * devuelve un array con el mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir, ser numéricas, positivas y mayores a 0 en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen, sus valores son numéricos, positivos y mayores a 0.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers_mayor_0(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers_positivos($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        if(is_object($data)){
            $data = (array) $data;
        }
        foreach($keys as $key){
            if ((float)$data[$key] <= 0.0) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero positivo mayor a 0',
                    $data);
            }
        }
        return true;

    }

    /**
     * TRASLADADO
     * Valida que los valores de las claves especificadas en un array sean números positivos.
     *
     * Esta función toma un array de claves y un conjunto de datos. Primero valida que las claves
     * especificadas existan en los datos y que sus valores sean numéricos. Luego, verifica que
     * los valores sean números positivos. Si ocurre un error durante la validación de las claves,
     * si algún valor no es numérico, o si algún valor no es positivo, se devuelve un array con el
     * mensaje de error correspondiente.
     *
     * @param array $keys Arreglo de claves que deben existir, ser numéricas y positivas en los datos.
     * @param array|object $data Conjunto de datos en el que se validarán las claves.
     *
     * @return bool|array Devuelve true si todas las claves existen, sus valores son numéricos y positivos.
     *                    Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function valida_numbers_positivos(array $keys, $data)
    {
        $valida = $this->valida_keys($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        $valida = $this->valida_numbers($keys, $data);
        if(error::$en_error){
            return $this->error->error('Error al validar $keys', $valida);
        }
        if(is_object($data)){
            $data = (array) $data;
        }
        foreach($keys as $key){
            if ((float)$data[$key] < 0.0) {
                return $this->error->error('Error $data[$key] '.$key.' Debe ser un numero positivo', $data);
            }
        }
        return true;

    }

    /**
     * EM3
     * Valida si un texto cumple con un patrón de expresión regular especificado.
     *
     * Este método verifica si un texto dado cumple con un patrón de expresión regular asociado a una clave en un array.
     * Si la clave o el patrón no existen, o si están vacíos, devuelve un error. Si el texto coincide con el patrón,
     * devuelve `true`; de lo contrario, devuelve `false`.
     *
     * @param string $key Clave que identifica el patrón dentro del array `$patterns`. No puede estar vacía.
     * @param array $patterns Array asociativo donde las claves son identificadores y los valores son expresiones regulares.
     * @param string $txt Texto que se desea validar contra el patrón.
     *
     * @return bool|array Devuelve `true` si el texto coincide con el patrón, `false` si no coincide.
     *                    En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Validación de un email
     * ```php
     * $key = 'email';
     * $patterns = [
     *     'email' => '/^[\w\.-]+@[\w\.-]+\.[a-zA-Z]{2,6}$/'
     * ];
     * $txt = 'usuario@ejemplo.com';
     *
     * $resultado = $this->valida_pattern($key, $patterns, $txt);
     * // Resultado esperado: true
     * ```
     *
     * @example Caso exitoso: Validación de un número de teléfono
     * ```php
     * $key = 'telefono';
     * $patterns = [
     *     'telefono' => '/^\d{10}$/'
     * ];
     * $txt = '1234567890';
     *
     * $resultado = $this->valida_pattern($key, $patterns, $txt);
     * // Resultado esperado: true
     * ```
     *
     * @example Caso fallido: Texto no coincide con el patrón
     * ```php
     * $key = 'telefono';
     * $patterns = [
     *     'telefono' => '/^\d{10}$/'
     * ];
     * $txt = '12345';
     *
     * $resultado = $this->valida_pattern($key, $patterns, $txt);
     * // Resultado esperado: false
     * ```
     *
     * @example Caso de error: Clave vacía
     * ```php
     * $key = '';
     * $patterns = [
     *     'telefono' => '/^\d{10}$/'
     * ];
     * $txt = '1234567890';
     *
     * $resultado = $this->valida_pattern($key, $patterns, $txt);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $key esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso de error: Patrón no encontrado
     * ```php
     * $key = 'email';
     * $patterns = [];
     * $txt = 'usuario@ejemplo.com';
     *
     * $resultado = $this->valida_pattern($key, $patterns, $txt);
     * // Resultado esperado:
     * // [
     * //     'error' => '$patterns[email] no existe',
     * //     'data' => []
     * // ]
     * ```
     */
    final public function valida_pattern(string $key, array $patterns, string $txt){
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error $key esta vacio', $key);
        }
        if(!isset($patterns[$key])){
            return $this->error->error('$patterns['.$key.'] no existe', $patterns);
        }
        $regex = $patterns[$key];
        if($regex === ''){
            return $this->error->error('$patterns['.$key.'] esta vacio', $patterns);
        }
        $result = preg_match($regex, $txt);
        $r = false;
        if((int)$result !== 0){
            $r = true;
        }
        return $r;

    }

    /**
     * TRASLADADO
     * Valida el patrón de un campo específico en un modelo.
     *
     * Esta función se encarga de validar que un campo dentro de un modelo cumpla
     * con un patrón específico definido por el tipo de campo.
     * Primero, verifica que las claves `$key` y `$tipo_campo` no estén vacías.
     * Luego, asegura que el registro dentro del modelo no esté vacío.
     * Finalmente, si el campo en cuestión tiene un valor, se valida contra
     * el patrón especificado.
     *
     * @param string $key La clave del campo que se desea validar.
     * @param object $modelo El objeto del modelo que contiene el registro a validar.
     * @param string $tipo_campo El tipo de campo que se utilizará para la validación del patrón.
     * @return bool|array Devuelve `true` si la validación es exitosa. En caso de error,
     *                    retorna un objeto de la clase `error` con el detalle del problema.
     */
    final public function valida_pattern_campo(string $key, object $modelo, string $tipo_campo)
    {

        $key = trim($key);
        if($key === ''){
            return (new error())->error('Error $key esta vacio',$key);
        }
        $tipo_campo = trim($tipo_campo);
        if($tipo_campo === ''){
            return (new error())->error('Error $tipo_campo esta vacio',$tipo_campo);
        }

        if(sizeof($modelo->registro) === 0){
            return (new error())->error('Error el registro  no puede venir vacio',$modelo->registro);

        }
        if(isset($modelo->registro[$key])&&(string)$modelo->registro[$key] !==''){
            $valida_data = $this->valida_pattern_modelo($key,$modelo,$tipo_campo);
            if(error::$en_error){
                return (new error())->error('Error al validar',$valida_data);
            }
        }
        return true;
    }


    /**
     * TRASLADADO
     * Valida que el valor de un campo del modelo cumpla con un patrón específico.
     *
     * Esta función realiza las siguientes validaciones:
     * 1. Verifica si la clave del campo y el tipo de campo no están vacíos.
     * 2. Verifica si el campo existe en el registro del modelo.
     * 3. Verifica si el patrón para el tipo de campo especificado existe en el modelo.
     * 4. Compara el valor del campo con el patrón utilizando expresiones regulares.
     * 5. Retorna un array de error si alguna de las validaciones falla.
     * 6. Retorna `true` si todas las validaciones pasan.
     *
     * @param string $key La clave del campo a validar.
     * @param object $modelo El objeto del modelo que contiene el registro y los patrones.
     * @param string $tipo_campo El tipo de campo que determina el patrón a utilizar.
     * @return bool|array Retorna `true` si el valor del campo cumple con el patrón; de lo contrario, retorna un array de error.
     */
    private function valida_pattern_modelo(string $key, object $modelo, string $tipo_campo)
    {
        $key = trim($key);
        if($key === ''){
            return (new error())->error('Error $key esta vacio',$key);
        }
        $tipo_campo = trim($tipo_campo);
        if($tipo_campo === ''){
            return (new error())->error('Error $tipo_campo esta vacio',$tipo_campo);
        }
        if(!isset($modelo->registro[$key])){
            return (new error())->error('Error no existe el campo '.$key,$modelo->registro);
        }
        if(!isset($modelo->patterns[$tipo_campo])){
            return (new error())->error('Error no existe el pattern '.$tipo_campo,$modelo->registro);
        }
        $value = trim($modelo->registro[$key]);
        $pattern = trim($modelo->patterns[$tipo_campo]);

        if(!preg_match($pattern, $value)){
            return (new error())->error('Error el campo '.$key.' es invalido',
                [$modelo->registro[$key],$pattern]);
        }
        return true;
    }

    /**
     * TRASLADADO
     * Valida si una transacción puede ser realizada sobre un registro activo.
     *
     * Esta función verifica si una transacción puede ser realizada sobre un registro.
     * Si el modelo no aplica la transacción cuando está inactivo, se valida el ID del registro,
     * se obtiene el registro y se verifica su estado. Si el registro está inactivo,
     * se devuelve un array con el error correspondiente.
     *
     * @param object $modelo El modelo que contiene los datos y métodos necesarios para la validación.
     *
     * @return bool|array `true` si la transacción puede ser realizada, o un array de error en caso contrario.
     */
    final public function valida_transaccion_activa(object $modelo)
    {
        if(!$modelo->aplica_transaccion_inactivo){
            if($modelo->registro_id <= 0){
                return (new error())->error('Error el id debe ser mayor a 0',
                    $modelo->registro_id);
            }
            $registro = $modelo->obten_data();
            if(error::$en_error){
                return (new error())->error('Error al obtener registro', $registro);
            }
            if(!isset($registro[$modelo->tabla.'_status'])){
                return (new error())->error(
                    'Error no existe el registro con el campo '.$modelo->tabla.'_status', $registro);
            }
            if($registro[$modelo->tabla.'_status'] === 'inactivo'){
                return (new error())->error(
                    'Error el registro no puede ser manipulado', $registro);
            }
        }
        return true;
    }

    /**
     * TRASLADADO
     * Valida que los valores de las claves especificadas en un arreglo sean válidos.
     *
     * Esta función verifica que las claves proporcionadas existan en el arreglo y que sus valores coincidan
     * con uno de los valores permitidos especificados en $arreglo_values.
     *
     * @param array $arreglo El arreglo que contiene los campos a validar.
     * @param array $arreglo_keys Las claves de los campos que deben ser validados.
     * @param array $arreglo_values Los valores permitidos para las claves especificadas.
     * @return string|array Devuelve una cadena con los valores válidos si todas las validaciones son exitosas,
     *                      o un array de error en caso de que alguna validación falle.
     */
    final public function valida_values_del_array(array $arreglo, array $arreglo_keys, array $arreglo_values){
        $texto_valido = '';
        foreach ($arreglo_keys as $key){
            $key = trim($key);
            if($key === ''){
                return $this->error->error('Error $key esta vacio', $arreglo_keys);
            }
            $encontrado = false;
            $texto_valido = "Los valores validos para campo [$key] son: ";
            foreach ($arreglo_values as $value){
                $texto_valido .= " '$value'";

                if(!isset($arreglo[$key])){
                    return $this->error->error('Error no existe el el arreglo key '.$key, $arreglo_keys);
                }

                if($arreglo[$key] === $value){
                    $encontrado = true;
                }
            }
            if($encontrado === false){
                return $this->error->error("$texto_valido", $arreglo);
            }
        }
        return $texto_valido;
    }

}