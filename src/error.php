<?php
namespace desarrollo_em3\error;
class error{

    public string $mensaje = 'Error desconocido';
    public string $mensaje_limpio = 'Error desconocido';
    public string $class;
    public $line;
    public $file;
    public $data;
    public $error;
    public $function;
    public $data_error;
    public $header;
    public static bool $en_error = false;


    /**
     * EM3
     * Genera un array detallado con información de un error, incluyendo datos de depuración.
     *
     * Este método se utiliza para estructurar y devolver información detallada sobre un error, incluyendo
     * la clase, archivo, función y línea donde ocurrió, así como datos adicionales proporcionados.
     * Es útil para manejar y registrar errores de manera consistente en el sistema.
     *
     * ### Propósito
     * - Estandarizar la estructura de los errores para facilitar su manejo y depuración.
     * - Incluir información contextual del error, como la clase, función, archivo y línea.
     * - Incorporar datos relacionados con errores de bases de datos o referencias adicionales.
     *
     * ### Proceso
     * - Valida y asigna los valores de los parámetros proporcionados.
     * - Si no se proporcionan ciertos valores (como `class`, `file`, `line`, `function`), los obtiene automáticamente del stack de depuración.
     * - Devuelve un array con la información estructurada del error.
     *
     * @param string $mensaje Mensaje descriptivo del error.
     * @param mixed $data Datos adicionales relacionados con el error.
     * @param string $class (Opcional) Nombre de la clase donde ocurrió el error. Se obtiene automáticamente si no se proporciona.
     * @param string $file (Opcional) Archivo donde ocurrió el error. Se obtiene automáticamente si no se proporciona.
     * @param string $function (Opcional) Nombre de la función donde ocurrió el error. Se obtiene automáticamente si no se proporciona.
     * @param string $line (Opcional) Línea de código donde ocurrió el error. Se obtiene automáticamente si no se proporciona.
     * @param int $n_error_mysql (Opcional) Número del error relacionado con MySQL.
     * @param string $error_mysql (Opcional) Mensaje del error de MySQL.
     * @param array $referencias (Opcional) Referencias adicionales relacionadas con el error.
     * @param bool $api_format (Opcional) Si es `true`, devuelve un array simplificado para uso en APIs.
     *
     * @return array Retorna un array estructurado con información detallada del error.
     *               Incluye claves como `error`, `mensaje`, `class`, `function`, `file`, `line`, `data`, entre otras.
     *
     * ### Ejemplo de uso
     *
     * #### Caso básico: Error genérico
     * ```php
     * $errorHandler = new error();
     * $resultado = $errorHandler->error('Error al procesar datos', ['detalle' => 'Datos inválidos']);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 1,
     * //     'mensaje' => '<font size=\'2\'><div><b style=\'color: brown\'>Error al procesar datos</b></div>',
     * //     'class' => '<div><b>NombreDeLaClase</b></div>',
     * //     'function' => '<div><b>NombreDeLaFuncion</b></div>',
     * //     'file' => '<div><b>/ruta/al/archivo.php</b></div>',
     * //     'line' => '<div><b>123</b></div><br></font>',
     * //     'data' => ['detalle' => 'Datos inválidos'],
     * //     'n_error_mysql' => 0,
     * //     'error_mysql' => '',
     * //     'referencias' => [],
     * //     'mensaje_limpio' => 'Error al procesar datos'
     * // ]
     * ```
     *
     * #### Caso con información adicional: Error de MySQL
     * ```php
     * $errorHandler = new error();
     * $resultado = $errorHandler->error(
     *     'Error al ejecutar consulta SQL',
     *     ['query' => 'SELECT * FROM tabla'],
     *     '',
     *     '',
     *     '',
     *     '',
     *     1064,
     *     'Syntax error in SQL statement'
     * );
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 1,
     * //     'mensaje' => '<font size=\'2\'><div><b style=\'color: brown\'>Error al ejecutar consulta SQL</b></div>',
     * //     'class' => '<div><b>NombreDeLaClase</b></div>',
     * //     'function' => '<div><b>NombreDeLaFuncion</b></div>',
     * //     'file' => '<div><b>/ruta/al/archivo.php</b></div>',
     * //     'line' => '<div><b>123</b></div><br></font>',
     * //     'data' => ['query' => 'SELECT * FROM tabla'],
     * //     'n_error_mysql' => 1064,
     * //     'error_mysql' => 'Syntax error in SQL statement',
     * //     'referencias' => [],
     * //     'mensaje_limpio' => 'Error al ejecutar consulta SQL'
     * // ]
     * ```
     *
     * #### Caso para API: Formato simplificado
     * ```php
     * $errorHandler = new error();
     * $resultado = $errorHandler->error('Error en la solicitud', [], '', '', '', '', 0, '', [], true);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 1,
     * //     'mensaje' => 'Error en la solicitud',
     * //     'data' => [],
     * //     'n_error_mysql' => 0,
     * //     'error_mysql' => '',
     * //     'referencias' => [],
     * //     'mensaje_limpio' => 'Error en la solicitud'
     * // ]
     * ```
     *
     * @note Esta función utiliza `debug_backtrace` para obtener información contextual automáticamente si no se proporciona.
     */
    final public function error(string $mensaje , $data,string $class = '',string $file = '',string $function = '',
                          string $line = '', int $n_error_mysql = 0, string $error_mysql = '',
                          array $referencias = array(), bool $api_format = false): array{

        $debug = debug_backtrace(2);

        $class = trim($class);

        if($class === ''){
            if(isset($debug[1]['class'])){
                $class = $debug[1]['class'];
            }
        }

        $file = trim($file);
        if($file === ''){
            if(isset($debug[0]['file'])){
                $file = $debug[0]['file'];
            }
        }

        $line = trim($line);
        if($line === ''){
            if(isset($debug[0]['line'])){
                $line = $debug[0]['line'];
            }
        }

        $function = trim($function);
        if($function === ''){
            if(isset($debug[1]['function'])){
                $function = $debug[1]['function'];
            }
        }

        $this->error = 1;
        $this->mensaje_limpio = trim($mensaje);
        $this->mensaje = "<font size='2'><div><b style='color: brown'>$mensaje</b></div>";
        $this->class = "<div><b>$class</b></div>";
        $this->function = "<div><b>$function</b></div>";
        $this->file = "<div><b>$file</b></div>";
        $this->line = "<div><b>$line</b></div><br></font>";
        $this->data = $data;

        $this->data_error = array('<font size="2">','mensaje'=>'Exito','class'=>$this->class,'function'=>$this->function,
            'file'=>$this->file,'line'=>$this->line,'</font><hr><font size="1">',
            'data'=>$this->data,'</font>','n_error_mysql'=>$n_error_mysql,'error_mysql'=>$error_mysql,
            'mensaje_limpio'=>$this->mensaje_limpio);
        if($this->error === 1){
            if($api_format === true){
                $this->data_error = array();
            }
            $this->data_error['error'] = 1;
            $this->data_error['mensaje'] = $this->mensaje;
            $this->data_error['data'] = $this->data;
            $this->data_error['n_error_mysql'] = $n_error_mysql;
            $this->data_error['error_mysql'] = $error_mysql;
            $this->data_error['referencias'] = $referencias;
            $this->data_error['mensaje_limpio'] = $this->mensaje_limpio;
        }
        self::$en_error = true;
        return $this->data_error;
    }

    final public function datos($error,$mensaje , $class, $line,$file,$data, $function,  $n_error_mysql = 0,
                          $error_mysql = '',$referencias = array(), $api_format = false): array{
        $this->error = $error;
        $this->mensaje = "<font size='2'><div><b style='color: brown'>$mensaje</b></div>";
        $this->class = "<div><b>$class</b></div>";
        $this->function = "<div><b>$function</b></div>";
        $this->file = "<div><b>$file</b></div>";
        $this->line = "<div><b>$line</b></div><br></font>";
        $this->data = $data;

        $this->data_error = array('<font size="2">','mensaje'=>'Exito','class'=>$this->class,'function'=>$this->function,
            'file'=>$this->file,'line'=>$this->line,'</font><hr><font size="1">',
            'data'=>$this->data,'</font>','n_error_mysql'=>$n_error_mysql,'error_mysql'=>$error_mysql);
        if($error === 1){
            if($api_format === true){
                $this->data_error = array();
            }
            //self::$en_error = true;
            $this->data_error['error'] = 1;
            $this->data_error['mensaje'] = $this->mensaje;
            $this->data_error['data'] = $this->data;
            $this->data_error['n_error_mysql'] = $n_error_mysql;
            $this->data_error['error_mysql'] = $error_mysql;
            $this->data_error['referencias'] = $referencias;
        }
        self::$en_error = true;
        return $this->data_error;
    }
    final public function es_error(string $mensaje, $data){
        $debug = debug_backtrace(2);
        $funcion_llamada = '';
        if(isset($debug[2]['function'])){
            $funcion_llamada = $debug[2]['function'];
        }
        $class_llamada = '';
        if(isset($debug[2]['class'])){
            $class_llamada = $debug[2]['class'];
        }
        $data_error['error'] = 1;
        $data_error['mensaje'] = '<b><span style="color:red">' . $mensaje . '</span></b>';
        $data_error['file'] = '<b>' . $debug[0]['file'] . '</b>';
        $data_error['line'] = '<b>' . $debug[0]['line'] . '</b>';
        $data_error['class'] = '<b>' . $debug[1]['class'] . '</b>';
        $data_error['function'] = '<b>' . $debug[1]['function'] . '</b>';
        $data_error['data'] = $data;

        $_SESSION['error_resultado'][] = $data_error;

        self::$en_error = true;
        $this->mensaje = $mensaje;
        $this->class = $debug[1]['class'];
        $this->line = $debug[0]['line'];
        $this->file = $debug[0]['file'];
        $this->function = $debug[1]['function'];
        if($data === null){
            $data = '';
        }
        $this->data = $data;
        return $data_error;
    }

}